#ifndef LINEAR_QUEUE_H
#define LINEAR_QUEUE_H

#include <stdio.h>
#include <stdlib.h>
#define MAX_QUEUE_SIZE 5

typedef int element;
typedef struct {
	int front;
	int rear;
	element data[MAX_QUEUE_SIZE];
}LinearQueueType;

void error(char* message);
void init_linear_queue(LinearQueueType* q);
void linearqueue_print(LinearQueueType* q);
int is_linear_full(LinearQueueType* q);
int is_linear_empty(LinearQueueType* q);
void linear_enqueue(LinearQueueType* q, int item);
int linear_dequeue(LinearQueueType* q);

#endif