#include "circle_queue.h"


void init_circle_queue(CircleQueueType* q) {
	q->front = q->rear = 0;
}

int is_circle_empty(CircleQueueType* q) {
	return (q->front == q->rear);
}

int is_circle_full(CircleQueueType* q) {
	return ((q->rear + 1) % MAX_QUEUE_SIZE == q->front);
}

void circlequeue_print(CircleQueueType* q) {
	printf("QUEUE(front= %d  rear= %d) = ", q->front, q->rear);
	if (!is_circle_empty(q)) {
		int i = q->front;
		do {
			i = (i + 1) % (MAX_QUEUE_SIZE);
			printf("%d | ", q->data[i]);
			if (i == q->rear)
				break;
		} while (i != q->front);
	}
	printf("\n");
}

void circle_enqueue(CircleQueueType* q, element item) {
	if (is_circle_full(q)) {
		error("큐가 포화상태입니다.");
	}
	q->rear = (q->rear + 1) % MAX_QUEUE_SIZE;
	q->data[q->rear] = item;
}

element circle_dequeue(CircleQueueType* q) {
	if (is_circle_empty(q)) {
		error("큐가 공백상태입니다.");
	}
	q->front = (q->front + 1) % MAX_QUEUE_SIZE;
	return q->data[q->front];
}

element peak(CircleQueueType* q) {
	if (is_circle_empty(q)) {
		error("큐가 공백상태입니다.");
	}
	return q->data[q->front + 1] % MAX_QUEUE_SIZE;
}