#ifndef THREAD_BINARY_TREE_H
#define THREAD_BINARY_TREE_H

#include <stdio.h>
#include <stdlib.h>
#define TRUE 1
#define FALSE 0

typedef struct{
	int data;
	struct ThreadTreeNode* left, * right;
	int is_thread;
}ThreadTreeNode;

ThreadTreeNode* find_successor(ThreadTreeNode* p);
void thread_inorder(ThreadTreeNode* t);
#endif