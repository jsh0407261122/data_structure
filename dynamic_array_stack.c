#include "dynamic_array_stack.h"

void init_dynamic_stack(DynamicStackType* s) {
	s->top = -1;
	s->capacity = 1;
	s->data = (element*)malloc(s->capacity * sizeof(element));
}

int is_dynamic_empty(DynamicStackType* s) {
	return (s->top == -1);
}

int is_dynamic_full(DynamicStackType* s) {
	return (s->top == (s->capacity - 1));
}

void push_dynamic(DynamicStackType* s, element item) {
	if (is_dynamic_full(s)) {
		s->capacity *= 2;
		s->data = (element*)realloc(s->data, s->capacity * sizeof(element));
	}
	s->data[++(s->top)] = item;
}

element pop_dynamic(DynamicStackType* s) {
	if (is_dynamic_empty(s)) {
		fprintf(stderr, "스택 공백 에러\n");
		exit(1);
	}
	else return s->data[(s->top)--];
}