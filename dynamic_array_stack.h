#ifndef DYNAMIC_ARRAY_STACK_H
#define DYNAMIC_ARRAY_STACK_H

#include <stdio.h>
#include <stdlib.h>
#define MAX_STACK_SIZE 100

typedef int element;
typedef struct {
	element* data;
	int capacity;
	int top;
}DynamicStackType;

void init_dynamic_stack(DynamicStackType* s);
int is_dynamic_empty(DynamicStackType* s);
int is_dynamic_full(DynamicStackType* s);
void push_dynamic(DynamicStackType* s, element item);
element pop_dynamic(DynamicStackType* s);

#endif

