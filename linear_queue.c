#include "linear_queue.h"

void error(char* message) {
	fprintf(stderr, "%s\n", message);
	exit(1);
}

void init_linear_queue(LinearQueueType* q) {
	q->rear = -1;
	q->front = -1;
}

void linearqueue_print(LinearQueueType* q) {
	for (int i = 0; i < MAX_QUEUE_SIZE; i++) {
		if (i <= q->front || i > q->rear)
			printf("   |");
		else
			printf("%d |", q->data[i]);
	}
	printf("\n");
}

int is_linear_full(LinearQueueType* q) {
	if (q->rear == MAX_QUEUE_SIZE - 1)
		return 1;
	else
		return 0;
}

int is_linear_empty(LinearQueueType* q) {
	if (q->front == q->rear)
		return 1;
	else
		return 0;
}

void linear_enqueue(LinearQueueType* q, int item) {
	if (is_linear_full(q)) {
		error("큐가 포화상태입니다.");
		return;
	}
	q->data[++(q->rear)] = item;
}

int linear_dequeue(LinearQueueType* q) {
	if (is_linear_empty(q)) {
		error("큐가 공백상태입니다.");
		return -1;
	}
	int item = q->data[++(q->front)];
	return item;
}
