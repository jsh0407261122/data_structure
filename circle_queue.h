#ifndef CIRCLE_QUEUE_H
#define CIRCLE_QUEUE_H

#include <stdio.h>
#include <stdlib.h>
#define MAX_QUEUE_SIZE 5

typedef int element;
typedef struct {
	element data[MAX_QUEUE_SIZE];
	int front;
	int rear;
}CircleQueueType;

void init_circle_queue(CircleQueueType* q);
int is_circle_empty(CircleQueueType* q);
int is_circle_full(CircleQueueType* q);
void circlequeue_print(CircleQueueType* q);
void circle_enqueue(CircleQueueType* q, element item);
element circle_dequeue(CircleQueueType* q);
element peak(CircleQueueType* q);

#endif