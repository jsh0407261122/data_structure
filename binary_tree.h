#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int data;
	struct TreeNode* left, * right;
}TreeNode;

void inorder(TreeNode* root);
void preorder(TreeNode* root);
void postorder(TreeNode* root);
#endif