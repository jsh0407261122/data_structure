#include "circular_linked_list.h"

void print_clist(CListNode* head) {
	CListNode* p;
	if (head == NULL) return;
	p = head->link;
	do {
		printf("%d -> ", p->data);
		p = p->link;
	} while (p != head->link);
}

CListNode* cinsert_first(CListNode* head, element data) {
	CListNode* node = (CListNode*)malloc(sizeof(CListNode));
	node->data = data;
	if (head == NULL) {
		head = node;
		node->link = head;
	}
	else {
		node->link = head->link;
		head->link = node;
	}
	return head;
}

CListNode* cinsert_last(CListNode* head, element data) {
	CListNode* node = (CListNode*)malloc(sizeof(CListNode));
	node->data = data;
	if (head == NULL) {
		head = node;
		node->link = head;
	}
	else {
		node->link = head->link;
		head->link = node;
		head = node;
	}
	return head;
}

