#include <stdio.h>
#include "array_stack.h"
#include "dynamic_array_stack.h"
#include "linear_queue.h"
#include "circle_queue.h"
#include "singly_linked_list.h"
#include "doubly_linked_list.h"
#include "circular_linked_list.h"
#include "binary_tree.h"
#include "thread_binarytree.h"

void execute_list(int num) {
	switch (num) {
		case 1:
			printf("단순연결리스트를 선택하셨습니다.\n");
			printf("단순연결리스트를 통해 데이터 삽입,삭제를 출력합니다.\n\n");

			ListNode* head = NULL;

			for (int i = 0; i < 5; i++) {
				head = insert_first(head, i);
				print_list(head);
			}
			for (int i = 0; i < 5; i++) {
				head = delete_first(head);
				print_list(head);
			}
			printf("\n");
			break;
		case 2:
			printf("원형연결리스트를 선택하셨습니다.\n");
			printf("원형연결리스트를 통해 데이터를 삽입하고 출력합니다.\n\n");

			CListNode* chead = NULL;

			chead = cinsert_last(chead, 20);
			chead = cinsert_last(chead, 30);
			chead = cinsert_last(chead, 40);
			chead = cinsert_first(chead, 10);
			print_clist(chead);
			printf("\n");
			break;
		case 3:
			printf("이중연결리스트를 선택하셨습니다.\n");
			printf("이중연결리스트를 통해 데이터 삽입,삭제를 출력합니다.\n\n");

			DListNode* dhead = (DListNode*)malloc(sizeof(DListNode));

			init(dhead);
			printf("추가 단계\n");
			for (int i = 0; i < 5; i++) {
				dinsert(dhead, i);
				print_dlist(dhead);
			}
			printf("\n삭제 단계\n");
			for (int i = 0; i < 5; i++) {
				print_dlist(dhead);
				ddelete(dhead, dhead->rlink);
			}
			free(dhead);
			printf("\n");
			break;
		default:
			printf("잘못선택하셨습니다.");
			break;
	}
}
void execute_stack(int num) {
	switch (num) {
		case 1:
			printf("배열스택을 선택하셨습니다.\n");
			printf("스택에 1, 2, 3을 순서대로 넣고, 최상위 요소를 제거하면서 그 값을 출력합니다.\n\n");

			StackType s;
			init_stack(&s);
			push(&s, 1);
			push(&s, 2);
			push(&s, 3);
			printf("%d \n", pop(&s));
			printf("%d \n", pop(&s));
			printf("%d \n", pop(&s));
			printf("\n");
			break;
		case 2:
			printf("동적배열스택을 선택하셨습니다\n");
			printf("스택에 1, 2, 3을 순서대로 넣고, 최상위 요소를 제거하면서 그 값을 출력합니다.\n\n");

			DynamicStackType ds;
			init_dynamic_stack(&ds);
			push_dynamic(&ds, 1);
			push_dynamic(&ds, 2);
			push_dynamic(&ds, 3);
			printf("%d \n", pop_dynamic(&ds));
			printf("%d \n", pop_dynamic(&ds));
			printf("%d \n", pop_dynamic(&ds));
			printf("\n");
			break;
		default:
			printf("잘못선택하셨습니다.");
			break;
	}
}
void execute_queue(int num) {
	switch (num) {
		case 1:
			printf("선형큐를 선택하셨습니다.\n");
			printf("10,20,30을 순서대로 큐에 넣고, 요소를 제거하고 그 값을 출력합니다.\n\n");

			int item = 0;
			LinearQueueType q;
			init_linear_queue(&q);
			linear_enqueue(&q, 10); linearqueue_print(&q);
			linear_enqueue(&q, 20); linearqueue_print(&q);
			linear_enqueue(&q, 30); linearqueue_print(&q);
			item = linear_dequeue(&q); linearqueue_print(&q);
			item = linear_dequeue(&q); linearqueue_print(&q);
			item = linear_dequeue(&q); linearqueue_print(&q);
			printf("\n");
			break;
		case 2:
			printf("원형큐를 선택하셨습니다. \n");
	
			int element;
			CircleQueueType cq;
			init_circle_queue(&cq);
			printf("-----데이터 추가 단계-----\n");
			while (!is_circle_full(&cq)) {
				printf("정수를 입력하시오: ");
				scanf_s("%d", &element);
				circle_enqueue(&cq, element);
				circlequeue_print(&cq);
			}
			printf("큐는 포화상태입니다. \n\n");
	
			printf("-----데이터 삭제 단계-----\n");
			while (!is_circle_empty(&cq)) {
				element = circle_dequeue(&cq);
				printf("꺼내진 정수: %d\n", element);
				circlequeue_print(&cq);
			}
			printf("큐는 공백상태입니다.\n");
			break;
		default:
			printf("잘못선택하셨습니다.");
			break;
	}
}
void execute_tree(int num) {
	switch (num) {
	case 1:
		printf("이진트리를 선택하셨습니다.\n");

		TreeNode n1 = { 1,NULL,NULL };
		TreeNode n2 = { 4,&n1,NULL };
		TreeNode n3 = { 16,NULL,NULL };
		TreeNode n4 = { 25,NULL,NULL };
		TreeNode n5 = { 20,&n3,&n4 };
		TreeNode n6 = { 15,&n2,&n5 };
		TreeNode* root = &n6;

		int n=0;
		printf("1. 전위순회\n");
		printf("2. 중위순회\n");
		printf("3. 후위순회\n");
		printf(">> ");
		scanf_s("%d", &n);

		switch (n)
		{
			case 1:
				printf("이진트리의 전위순회를 선택하셨습니다.\n\n");
				preorder(root);
				printf("\n");
				break;
			case 2:
				printf("이진트리의 중위순회를 선택하셨습니다.\n\n");
				inorder(root);
				break;
			case 3:
				printf("이진트리의 후위순회를 선택하셨습니다.\n\n");
				postorder(root);
				break;
		}
		break;
	case 2:
		printf("스레드 이진트리를 선택하셨습니다.\n");

		ThreadTreeNode tn1 = { 'A',NULL,NULL,1 };
		ThreadTreeNode tn2 = { 'B',NULL,NULL,1 };
		ThreadTreeNode tn3 = { 'C',&tn1,&tn2,0 };
		ThreadTreeNode tn4 = { 'D',NULL,NULL,1 };
		ThreadTreeNode tn5 = { 'E',NULL, NULL,0 };
		ThreadTreeNode tn6 = { 'F',&tn4,&tn5,0 };
		ThreadTreeNode tn7 = { 'G',&tn3,&tn6,0 };
		ThreadTreeNode* texp = &tn7;
		tn1.right = &tn3;
		tn2.right = &tn7;
		tn4.right = &tn6;

		thread_inorder(texp);
		printf("\n");
		break;
	default:
		printf("잘못선택하셨습니다.");
		break;
	}
}

int main() {
	int num;
	
	printf("원하시는 자료구조를 선택해주세요.\n\n");
	printf("1. 리스트(list)\n");
	printf("2. 스택(stack)\n");
	printf("3. 큐(queue)\n");
	printf("4. 트리(tree)\n");
	printf(">> ");
	scanf_s("%d", &num);
	printf("\n");

	switch (num) {
		case 1:
			printf("1. 단순연결리스트\n");
			printf("2. 원형연결리스트\n");
			printf("3. 이중연결리스트\n");
			printf(">> ");
			scanf_s("%d", &num); printf("\n");
			execute_list(num);
			break;
		case 2:
			printf("1. 배열스택\n");
			printf("2. 동적배열스택\n");
			printf(">> ");
			scanf_s("%d", &num);
			execute_stack(num);
			break;
		case 3:
			printf("1. 선형큐\n");
			printf("2. 원형큐\n");
			printf(">> ");
			scanf_s("%d", &num); printf("\n");
			execute_queue(num);
			break;
		case 4:
			printf("1. 이진트리(전위순휘, 중위순회, 후위순회)\n");
			printf("2. 스레드 이진트리\n");
			printf(">> ");
			scanf_s("%d", &num); printf("\n");
			execute_tree(num);
			break;
		default:
			printf("잘못선택하셨습니다.");
			break;
	}
	return 0;
}