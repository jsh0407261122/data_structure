#ifndef CIRCULAR_LINKED_LIST_H
#define CIRCULAR_LINKED_LIST_H

#include <stdio.h>
#include <stdlib.h>

typedef int element;
typedef struct {
	element data;
	struct CListNode* link;
}CListNode;

void print_clist(CListNode* head);
CListNode* cinsert_first(CListNode* head, element data);
CListNode* cinsert_last(CListNode* head, element data);
#endif
