#include "thread_binarytree.h"

ThreadTreeNode* find_successor(ThreadTreeNode* p) {
	ThreadTreeNode* q = p->right;
	if (q == NULL || p->is_thread == TRUE)return q;

	while (q->left != NULL) q = q->left;
	return q;
}
void thread_inorder(ThreadTreeNode* t) {
	ThreadTreeNode* q;

	q = t;
	while (q->left)q = q->left;
	do {
		printf("%c -> ", q->data);
		q = find_successor(q);
	} while (q);
}