#ifndef SINGLY_LINKED_LIST_H
#define SINGLY_LINKED_LIST_H

#include <stdio.h>
#include <malloc.h>

typedef struct ListNode {
	int data;
	struct ListNode* link;
}ListNode;
ListNode* insert_first(ListNode* head, int value);
ListNode* insert(ListNode* head, ListNode* pre, int value);
ListNode* delete_first(ListNode* head);
ListNode* delete(ListNode* head, ListNode* pre);
void print_list(ListNode* head);

#endif